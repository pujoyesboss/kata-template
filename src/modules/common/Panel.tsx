import * as React from 'react';

interface HeadingProps {
  children: any;
}

interface BodyProps {
  children: any;
}

interface PanelProps {
  bsStyle?: string; 
  children: any;
}

const Heading = (props: HeadingProps) => (
  <div className="panel-heading">
    {props.children}
  </div>
);

const Body = (props: BodyProps) => (
  <div className="panel-body">
    {props.children}
  </div>
);

class Panel extends React.Component<PanelProps, any> {

  static Heading = Heading;
  static Body = Body;

  render() {
    return (
      <div className={'panel ' + (this.props.bsStyle || 'panel-default panel-shadow')}>
        <div className="sidebar-menu-inner">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Panel;