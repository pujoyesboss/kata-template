import * as React from 'react';

interface Props {
  children: any;
}

export default (props: Props) => (
  <div className="sidebar-menu toggle-others fixed">
    <div className="sidebar-menu-inner">
      {props.children}
    </div>
  </div>
);