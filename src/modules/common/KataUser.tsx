import * as React from 'react';

import LogoutButton from '../auth/LogoutButton';
const NoAvatar = require('../common/img/no-avatar.png');

interface Props {
  avatar?: string;
  username: string;
}

export default (props: Props) => (
  <div className="kata-user">
    <div className="kata-user-avatar">
      <img src={props.avatar && props.avatar || NoAvatar} />
    </div>
    <div className="kata-user-name">Hello {props.username}</div>
    <div className="kata-user-links fw4">
      <LogoutButton />
    </div>
  </div>
);