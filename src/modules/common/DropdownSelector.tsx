import * as React from 'react';
import {Dropdown, MenuItem} from 'react-bootstrap';
import * as _ from 'lodash';

interface Props {
  index: Array<string>;
  data: any;
  id: string;
  title: string;
  className?: string;
  bsStyle?: string;
  customStyle?: any;
  selectAction?: Function;
}

interface State {
  selected: string;
}

class Selector extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      selected: props.index[0]
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (!_.isEqual(nextProps.index, this.props.index) && nextProps.index.length > 0) {
      // this.setState({selected: nextProps.index[0]});
      this.onSelect(nextProps.index[0]);
    }
  }

  onSelect = (key: any) => {
    this.setState({selected: key});
    if (this.props.selectAction) {
      this.props.selectAction(key);
    }
  }

  public render() {
    const {index, data, id, title, className, bsStyle, customStyle} = this.props;
    const {selected} = this.state;
    return index.length > 0 && (
      <Dropdown id={id} onSelect={this.onSelect}>
        <Dropdown.Toggle bsStyle={bsStyle} className={className} style={customStyle}>
          {title}: {data[selected].name}&nbsp;&nbsp;
        </Dropdown.Toggle>
        <Dropdown.Menu className={'no-spacing dropdown-default'}>
          {index.map(key => <MenuItem key={key} eventKey={key}>{data[key].name}</MenuItem>)}
        </Dropdown.Menu>
      </Dropdown>
    ) || null;
  }
}

export default Selector;