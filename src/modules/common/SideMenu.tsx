import * as React from 'react';

const CustomScroll = require('react-custom-scroll');

interface Props {
  children: any;
}

export default (props: Props) => (
  <CustomScroll heightRelativeToParent="calc(100% - 100px)">
    <ul id="main-menu" className="main-menu">
      {props.children}
    </ul>
  </CustomScroll>
);