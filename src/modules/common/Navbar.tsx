import * as React from 'react';

interface Props {
  children: any;
}

export default (props: Props) => (
  <nav className="navbar user-info-navbar fixed" role="navigation">
    {props.children}
  </nav>
);