import * as React from 'react';

interface Props {
  children: any;
}

export default (props: Props) => 
  <div className="page-container">{props.children}</div>;