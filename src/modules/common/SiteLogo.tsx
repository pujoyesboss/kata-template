import * as React from 'react';

const Logo = require('../common/img/logo-white.png');
const LogoShort = require('../common/img/logo-white-short.png');

export default (props: any) => (
  <header className="logo-env">
    <div className="logo"> 
      <a href="" className="logo-expanded"> 
        <img src={Logo} width="110" alt=""/> 
      </a>  
      <a href="" className="logo-collapsed"> 
        <img src={LogoShort} width="40" alt=""/> 
      </a>
    </div>
  </header>
);