import * as React from 'react';

interface Props {
  title: string;
  desc?: string;
  children?: any;
}

export default (props: Props) => (
  <div className="page-title">
    <div className="title-env">
      <h1 className="title text-gray">{props.title}</h1>
      {props.desc && <p className="desc">{props.desc}</p>}
    </div>
    {props.children}
  </div>
);