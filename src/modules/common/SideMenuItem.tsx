import * as React from 'react';
import { NavLink } from 'react-router-dom';

interface Props {
  path: string;
  exact?: boolean;
  icon: string;
  title: string;
}

export default (props: Props) => (
  <li>
    <NavLink activeClassName="active" to={props.path} exact={props.exact}>
      <i className={'icon-' + props.icon} />
      <span className="title">{props.title}</span>
    </NavLink> 
  </li>
);