import * as React from 'react';

interface Props {
  title: string;
  style?: any;
}

export default (props: Props) => (
  <div className="sidebar-header" style={props.style}>{props.title}</div>
);