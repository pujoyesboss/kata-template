import * as React from 'react';

import { Panel, TitleBar } from '../common';
// import DeploymentCreate from './DeploymentCreate.Container';
// import DeploymentRefresh from './DeploymentRefresh.Container';
// import DeploymentList from './DeploymentList.Container';

export default ({match}: any) => (
  <div>
    <TitleBar title="Module4" desc="All about module 4">
      <div className="breadcrumb-env">
        {/* <DeploymentCreate />
        <DeploymentRefresh /> */}
      </div>
    </TitleBar>
    <Panel>
      <Panel.Body>
        {/* <DeploymentList /> */}
        Module 4 content
      </Panel.Body>
    </Panel>
  </div>
);