import * as React from 'react';

import { Panel, TitleBar } from '../common';
// import DeploymentCreate from './DeploymentCreate.Container';
// import DeploymentRefresh from './DeploymentRefresh.Container';
// import DeploymentList from './DeploymentList.Container';

export default ({match}: any) => (
  <div>
    <TitleBar title="Module1" desc="All about module 1">
      <div className="breadcrumb-env">
        {/* <DeploymentCreate />
        <DeploymentRefresh /> */}
      </div>
    </TitleBar>
    <Panel>
      <Panel.Body>
        {/* <DeploymentList /> */}
        Module 1 content
      </Panel.Body>
    </Panel>
  </div>
);