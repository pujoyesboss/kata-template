import reducers from './stores/reducers';
import * as actions from './stores/actions';
import App from './App';

export {
  reducers,
  actions
};

export default App;