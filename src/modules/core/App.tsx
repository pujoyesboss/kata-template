import * as React from 'react';
import { Route } from 'react-router-dom';
import { Col } from 'react-bootstrap';

import Async from './Async';
import {
  KataUser,
  MainContent,
  Navbar,
  PageContainer,
  Sidebar,
  SideMenu,
  SideMenuItem,
  SideMenuTitle,
  SiteLogo
} from '../common';
import BotSelector from './BotSelector.Container';

const Module1 = (props: any) => 
  <Async component={System.import('../module1')} properties={props}/>;
const Module2 = (props: any) => 
  <Async component={System.import('../module2')} properties={props}/>;
const Module3 = (props: any) => 
  <Async component={System.import('../module3')} properties={props}/>;
const Module4 = (props: any) => 
  <Async component={System.import('../module4')} properties={props}/>;

const firstSideTitleStyle = {
  paddingTop: 5
};
class App extends React.Component<{}, null> {
  render() {
    return (
      <PageContainer>
        <Sidebar>
          <SiteLogo />
          <BotSelector />
          <SideMenu>
            <SideMenuTitle title="BOT" style={firstSideTitleStyle}/>
            <SideMenuItem title="Module 1" path="/" exact icon="wrench"/>
            <SideMenuItem title="Module2 " path="/module2" icon="layers"/>
            <SideMenuItem title="Module 3" path="/module3" icon="list"/>
            <SideMenuTitle title="ANALYTIC" />
            <SideMenuItem title="Module 4" path="/module4" icon="user"/>
          </SideMenu>
        </Sidebar>
        <MainContent>
          <Navbar>
            <Col xs={3} sm={3} className="text-right pull-right">
              <KataUser username="Admin" />
            </Col>
          </Navbar>
          <Route exact path="/" component={Module1} />
           <Route path="/module2" component={Module2} />
          <Route path="/module3" component={Module3} />
          <Route path="/module4" component={Module4} />
        </MainContent>
      </PageContainer>
    );
  }
}

export default App;
