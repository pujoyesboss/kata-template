import * as React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {bot as botAction} from './stores/actions';
import BotSelector from './BotSelector';

// interface Props {
//   isLoading: boolean;
//   error: string;
//   index: Array<string>;
//   data: any;
// }

class BotSelectorContainer extends React.Component<any, void> {

  redirectAction = () => {
    this.props.history.push('/');
  }

  render() {
    return <BotSelector {...this.props} redirectAction={this.redirectAction}/>;
  }
}

const mapStateToProps = ({bot}: any) => {
  return {
    isLoading: bot.isLoading,
    error: bot.error,
    index: bot.index,
    data: bot.data,
    selected: bot.selected
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    fetchAction: () => dispatch(botAction.fetch()),
    selectAction: (key: string) => dispatch(botAction.select(key))
  };
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(BotSelectorContainer));