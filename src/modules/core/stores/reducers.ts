import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import app from './app/reducers';
import auth from './auth/reducers';
import bot from './bot/reducers';

export default combineReducers({
  app,
  bot,
  auth,
  router: routerReducer
});