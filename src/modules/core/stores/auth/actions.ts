// import axios from 'axios';
import TYPES from './types';

// import { ZAUN_API } from '../urls';
const Cookies = require('cookies-js');

function requestHandler() {
  return {
    type: TYPES.REQUEST
  };
}

function errorHandler(message: string) {
  return {
    type: TYPES.ERROR,
    payload: {
      error: message
    }
  };
}

function resetHandler() {
  Cookies.expire('siteid');
  return {
    type: TYPES.RESET
  };
}

function loginSuccess(siteid: string) {
  return {
    type: TYPES.LOGIN,
    payload: {
      siteid
    }
  };
}

function login(username: string, password: string) {
  return (dispatch: Function, getState: Function) => {
    dispatch(requestHandler());
    return new Promise((resolve, reject) => {resolve('yes'); })
      .then(() => {
        Cookies.set('siteid', 'admin');
        dispatch(loginSuccess('admin'));
      })
      .catch(() => {
        dispatch(errorHandler('Username or password not matched'));
      });
  };
}

function checkLogin() {
  return (dispatch: Function, getState: Function) => {
    const {auth} = getState();
    if (!auth.isLogedin) {
      let siteid = Cookies.get('siteid');
      if (siteid === undefined) {
        return false;
      } else {
        dispatch(loginSuccess(siteid));
      }
    }
    return true;
  };
}

export default {
  requestHandler,
  errorHandler,
  resetHandler,
  login,
  checkLogin
};