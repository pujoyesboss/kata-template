import TYPES from './types';

interface IState {
  isLoading: boolean;
  isLogedin: boolean;
  error: string;
  siteid: string;
}

interface IAction {
  type: string;
  payload: any;
}

const initialState = {
  isLoading: false,
  isLogedin: false,
  error: '',
  siteid: ''
};

const app = (state: IState = initialState, action: IAction) => {
  switch (action.type) {
    case TYPES.REQUEST:
      return Object.assign({}, state, {
        isLoading: true
      });
    case TYPES.ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.payload.error
      });
    case TYPES.LOGIN: 
      return Object.assign({}, state, {
        isLogedin: true,
        isLoading: false,
        siteid: action.payload.siteid
      });
    case TYPES.RESET:
      return Object.assign({}, state, initialState);
    default:
      return state;
  }
};

export default app;