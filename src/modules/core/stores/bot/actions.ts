import TYPES from './types';
// import Request from '../request';
// import {Bot as BotType} from './reducers';

function formatTimezone(timezone?: any): string {
  let timezoneStr = '-';
  if (!!timezone) {
    timezoneStr = parseFloat(timezone).toFixed(2);
    timezoneStr = timezoneStr.replace('.', ':');
    if (timezone > 0) {
      timezoneStr = '+' + timezoneStr;
    }
    timezoneStr = 'GMT ' + timezoneStr;
  }
  return timezoneStr;
}

function requestHandler() {
  return {
    type: TYPES.REQUEST
  };
}

function errorHandler(message: string) {
  return {
    type: TYPES.ERROR,
    payload: {
      error: message
    }
  };
}

function clearError() {
  return {
    type: TYPES.ERROR_CLEAR
  };
}

function shouldFetch(state: any) {
  const bot = state.bot;
  if (!bot.data) {
    return true;
  } else if (bot.isLoading) {
    return false;
  } else {
    return !bot.isValid;
  }
}

function fetch() {
  return (dispatch: Function, getState: Function) => {
    if (!shouldFetch( getState() )) {
      return new Promise((resolve, reject) => {
        resolve('do nothing');
      });
    }
    dispatch(requestHandler());
    return new Promise((resolve, reject) => {resolve('yes'); })
      .then(() => {
        return dispatch(fetchSuccess([
          {
            id: 'pakjoni',
            name: 'pakjoni'
          }, {
            id: 'veronica',
            name: 'veronica'
          }
        ], 1, 10));
      })
      .catch(() => {
        dispatch(errorHandler('Ooops something wrong when fetching data'));
        throw 'error';
      });
  };
}

function fetchSuccess(raw: any, page: number, limit: number) {
  let index: Array<string> = [],
      data = {};
  raw.map((item: any) => {
    index.push(item.id);
    item.timezoneStr = formatTimezone(item.timezone);
    data[item.id] = item;
  });
  return {
    type: TYPES.FETCH,
    payload: {
      index,
      data,
      page,
      limit
    }
  };
}

function invalidate() {
  return {
    type: TYPES.INVALIDATE
  };
}

function select(selected: string) {
  return {
    type: TYPES.SELECT,
    payload: {
      selected
    }
  };
}

export default {
  requestHandler,
  errorHandler,
  clearError,
  fetch,
  invalidate,
  select
};