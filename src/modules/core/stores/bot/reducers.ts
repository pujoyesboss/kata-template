import TYPES from './types';

interface State {
  selected: string;
  isLoading: boolean;
  isValid: boolean;
  error: string;
  page: number;
  limit: number;
  index: Array<string>;
  data: any;
  lastUpdate: number;
}

interface Action {
  type: string;
  payload: any;
}

export interface Bot {
  id: string;
  name: string;
  version: string;
  desc: string;
  lang: string;
  timezone: number;
  timezoneStr: string;
  flows: any;
  nlus: any;
  methods: any;
  config: any;
}

const initialState = {
  selected: '',
  isLoading: false,
  isValid: false,
  error: '',
  page: 1,
  limit: 15,
  index: [],
  data: {},
  lastUpdate: Date.now()
};

function getSuccessState() {
  return {
    isLoading: false,
    lastUpdate: Date.now()
  };
}

function add(state: State, data: Bot) {
  let newIndex = [data.id, ...state.index];
  let newData = { ...state.data };
  newData[data.id] = Object.assign({}, data);

  return Object.assign( {}, state, {
    index: newIndex,
    data: newData
  }, getSuccessState());
}

function edit(state: State, data: Bot) {
  let newData = { ...state.data };
  newData[data.id] = Object.assign({}, newData[data.id], data);

  return Object.assign( {}, state, { 
    data: newData 
  }, getSuccessState());
}

function remove(state: State, id: string) {
  const {index} = state;
  return Object.assign({}, state, { 
    index: index.filter(i => i !== id) 
  }, getSuccessState());
}

const bot = (state: State = initialState, action: Action) => {
  const {type, payload} = action;
  switch (type) {
    case TYPES.REQUEST: 
      return Object.assign({}, state, {
        isLoading: true
      });
    case TYPES.ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: payload.error
      });
    case TYPES.ERROR_CLEAR: 
      return Object.assign({}, state, {
        error: ''
      });
    case TYPES.FETCH: 
      return Object.assign({}, state, {
        page: payload.page,
        limit: payload.limit,
        index: [...payload.index],
        data: Object.assign({}, state.data, payload.data),
        isValid: true
      }, getSuccessState());
    case TYPES.INVALIDATE: 
      return Object.assign({}, state, {
        isValid: false
      });
    case TYPES.SELECT: 
      return Object.assign({}, state, {
        selected: payload.selected
      });
    case TYPES.ADD: 
      return add(state, payload);
    case TYPES.EDIT:
      return edit(state, payload);
    case TYPES.DELETE:
      return remove(state, payload.id);
    default: 
      return state;
  }
};

export default bot;