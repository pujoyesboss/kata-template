import axios from 'axios';

import authAction from './auth/actions';
import { ZAUN_API } from './urls';

function request(type: string, url: string, data?: any) {
  return (dispatch: Function, getState: Function) => {
    const {auth} = getState();
    return axios({
      method: type,
      baseURL: ZAUN_API,
      url: url,
      data: data,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Bearer ${auth.siteid}`
      }
    })
      .then(resp => resp.data)
      .catch(error => {
        if (error.response && error.response.status === 403) {
          return dispatch(authAction.resetHandler());
        }
        throw error;
      });
  };
}

export default request;