import TYPES from './types';

interface IState {
  showSidebar: boolean;
}

interface IAction {
  type: string;
  payload: any;
}

const initialState = {
  showSidebar: true
};

const app = (state: IState = initialState, action: IAction) => {
  switch (action.type) {
    case TYPES.SIDEBAR_OPEN:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};

export default app;