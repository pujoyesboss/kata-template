import * as React from 'react';

interface IProps {
  properties?: any;
  component: Promise<any>;
}
class Async extends React.Component<IProps, any> {
  Component: any;

  componentWillMount() {
    this.props.component.then(Component => {
      this.Component = Component;
      try {
        this.forceUpdate();
      } catch (error) {
        // do nothing
      }
    });
  }
  public render () {
    const {properties} = this.props;
    return this.Component ? <this.Component.default {...properties}/> : null;
  }
}

/**
 * Example
 * const Home = (props: any) => 
 *  <Async component={System.import('../home/Home')} properties={props}/>;
 */

export default Async;