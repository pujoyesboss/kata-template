import * as React from 'react';
import {Dropdown, MenuItem, Button} from 'react-bootstrap';

interface Props {
  isLoading: boolean;
  error: string;
  index: Array<string>;
  data: any;
  selected: string;
  redirectAction?: Function;
  fetchAction: Function;
  selectAction: Function;
}

interface State {
  selected: string;
}

const labelStyle = {
  color: '#979898'
};

const caretStyle = {
  marginTop: 8
};

const menuStyle = {
  minWidth: 210
};

class BotSelector extends React.Component<Props, State> {

  componentDidMount() {
    this.props.fetchAction()
      .then(() => 
        this.props.selectAction(this.props.index[0])
      );
  }

  onSelect = (key: any) => {
    if (key === 'action-load') {
      // 
    } else if (key === 'action-create') {
      if (this.props.redirectAction) {
        this.props.redirectAction();
      }
    } else {
      this.props.selectAction(key);
    }
  }

  renderLoading() {
    return (
      <div className="pt4 ph4">
        <Button block disabled>
          Loading...
        </Button>
      </div>
    );
  }

  renderCreator() {
    return (
      <div className="pt4 ph4">
        <Button block>
          <i className="icon-plus v-mid"/> Create Bot
        </Button>
      </div>
    );
  }

  renderSelector() {
    const {index, data, selected} = this.props;
    return (
      <div className="pt4 ph4">
        <label style={labelStyle}>Active Bot</label>
        <Dropdown id="dropdown-bot-selector" className="btn-block" onSelect={this.onSelect}>
          <Dropdown.Toggle className="btn-block text-left pa3 b--none" noCaret>
            {data[selected].name}
            <span className="caret pull-right" style={caretStyle}/>
          </Dropdown.Toggle>
          <Dropdown.Menu className="dropdown-default with-beak b--none" style={menuStyle}>
            {index.map(key => <MenuItem key={key} eventKey={key}>{data[key].name}</MenuItem>)}
            {/*<MenuItem eventKey="action-load">
              Load more...
            </MenuItem>*/}
            <MenuItem divider />
            <MenuItem eventKey="action-create">
              <i className="icon-wrench v-mid"/> Manage Bot
            </MenuItem>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    );
  }

  render() {
    const {isLoading, index, data, selected} = this.props;
    if (isLoading) {
      return this.renderLoading();
    } else {
      return !data[selected] || index.length === 0 
        ? this.renderCreator()
        : this.renderSelector();
    }
  }
}

export default BotSelector;

/**
 * TODO:
 * - figure it out how to efficiently load all bot for bot selector
 * - create scrollable dropdown for bot selector
 * - figure it out how to efficiently and effectively switch between bot
 * - bot sort by what
 */