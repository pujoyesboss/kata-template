import * as React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

// Mock of an Auth method, can be replaced with an async call to the backend. Must return true or false
const PrivateRoute = ({ component: Component, isLoggedIn, ...rest }: any) => {
  return (
    <Route {...rest} render={props => (
      isLoggedIn ? (
        <Component {...props}/>
      ) : (
        <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }}/>
      )
    )}/>
  );
};

const mapStateToProps = ({auth}: any) => {
  return {
    isLoggedIn: auth.isLogedin
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivateRoute);