import * as React from 'react';
import {connect} from 'react-redux';

import {auth as authAction} from '../core/stores/actions';
import Login from './Login';

interface Props {
  isLoading: boolean;
  isLoggedIn: boolean;
  error: string;
  loginAction: Function;
  checkLogin: Function;
  location: any;
  match: any;
  history: any;
}

const LoginContainer = (props: Props) => (
  <Login {...props}/>
);

const mapStateToProps = ({auth}: any) => {
  return {
    isLoggedIn: auth.isLogedin,
    isLoading: auth.isLoading,
    error: auth.error
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    checkLogin: () => dispatch(authAction.checkLogin()),
    loginAction: (username: string, password: string) => dispatch(authAction.login(username, password))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);