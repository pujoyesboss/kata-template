import * as React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {auth as authAction} from '../core/stores/actions';

export const LogoutButton = withRouter(({ history, logout }: any) => (
  <a href="#" onClick={(e) => {
    e.preventDefault();
    logout();
    {/*history.push('/');*/}
  }}>Sign out</a>
));

const mapStateToProps = ({auth}: any) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    logout: () => dispatch(authAction.resetHandler())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoutButton);