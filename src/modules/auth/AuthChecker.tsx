import * as React from 'react';
import {connect} from 'react-redux';

import {auth as authAction} from '../core/stores/actions';

class AuthChecker extends React.Component<any, any> {

  componentDidMount() {
    this.props.checkLogin();
  }

  render() {
    return null;
  }
}

const mapStateToProps = ({auth}: any) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    checkLogin: () => dispatch(authAction.checkLogin())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthChecker);