import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { Button, FormGroup, FormControl, InputGroup, Checkbox, Alert } from 'react-bootstrap';

import './auth.css';

const Logo = require('../common/img/logo.png');

interface Props {
  isLoading: boolean;
  isLoggedIn: boolean;
  error: string;
  loginAction: Function;
  location?: any;
}

export default class Login extends React.Component<Props, {}> {
  state = {
    username: '',
    password: ''
  };

  login = (event: any) => {
    event.preventDefault();
    this.props.loginAction(this.state.username, this.state.password);
  }

  inputChange = (name: string) => {
    return (event: any) => {
      let newState = {};
      newState[name] = event.target.value;
      this.setState(newState);
    };
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const { isLoading, isLoggedIn, error } = this.props;
    const { username, password } = this.state;
    
    if (isLoggedIn) {
      return (
        <Redirect to={from}/>
      );
    }
    
    return (
      <div className="auth-wrapper">
        <div className="auth-container br4 pa4">
          <div className="text-center pb5 pt4">
            <img src={Logo} className="auth-logo" alt="logo" />
          </div>
          {error !== '' && <Alert bsStyle="danger">{error}</Alert>}
          <form onSubmit={this.login}>
            <FormGroup controlId="auth-form-username">
              <InputGroup bsSize="lg">
                <InputGroup.Addon>
                  <i className="fa fa-user auth-input-icon"/>
                </InputGroup.Addon>
                <FormControl type="text" placeholder="Username" className="auth-input" value={username}  onChange={this.inputChange('username')}/>
              </InputGroup>
            </FormGroup>
            <FormGroup controlId="auth-form-password">
              <InputGroup bsSize="lg">
                <InputGroup.Addon>
                  <i className="fa fa-lock auth-input-icon"/>
                </InputGroup.Addon>
                <FormControl type="password" placeholder="Password" className="auth-input" value={password} onChange={this.inputChange('password')}/>
              </InputGroup>
            </FormGroup>
            <Checkbox className="auth-label">
              Remember me
            </Checkbox>
            {isLoading 
            && <Button bsStyle="primary" bsSize="lg" block disabled className="br-pill mt4">Loading...</Button>  
            || <Button type="submit" bsStyle="primary" bsSize="lg" block className="br-pill mt4">Login</Button>
            }
          </form>
          <p className="text-center">Presented by <a href="https://Kata.ai">Kata.ai</a></p>
        </div>
        <div className="kata-bg-biglines-left" />
        <div className="kata-bg-biglines-right" />
      </div>
    );
  }
}