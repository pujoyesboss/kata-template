import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';

import reducers from './modules/core/stores/reducers';
import { history } from './route';

const devTools = window['devToolsExtension'];

export default createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)),
    devTools ? devTools() : (f: any) => f
  )  
);