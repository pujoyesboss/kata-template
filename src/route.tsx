import * as React from 'react';
// import * as System from 'systemjs';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import ProtectedRoute from './modules/auth/Route';
import AuthChecker from './modules/auth/AuthChecker';
import Login from './modules/auth/LoginContainer';
import App from './modules/core/App';

// Create a history of your choosing (we're using a browser history in this case)
export const history = createHistory();

const routes = () => {
  return(
    <ConnectedRouter history={history}>
      <div>
        <AuthChecker />
        <Switch>
          <Route exact path="/login" component={Login} />
          <ProtectedRoute path="/" component={App}/>
          <Route render={() => <Redirect to="/login" />} />
        </Switch>
      </div>
    </ConnectedRouter>
  );
};

export default routes;