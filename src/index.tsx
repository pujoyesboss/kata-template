import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import registerServiceWorker from './utils/registerServiceWorker';
import store from './store';
import Routes from './route';

import './lib/fonts/fontawesome/css/font-awesome.css';
import './lib/fonts/simple-line-icons/css/simple-line-icons.css';
import './lib/fonts/museo-sans/museo-sans.css';
import './lib/css/bootstrap.css';
import './lib/css/xenon.css';
import './lib/css/tachyon.css';
import './lib/css/index.css';
import './lib/css/react-dates.css';

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
