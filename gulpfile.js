var gulp = require('gulp');
var less = require('gulp-less'); 
 
/* Task to compile less */
gulp.task('compile-less', function() {  
  gulp.src(['./less/xenon.less', './less/bootstrap.less'])
    .pipe(less())
    .pipe(gulp.dest('./src/lib/css'));
}); 

/* Task when running `gulp` from terminal */
gulp.task('default', ['compile-less']);