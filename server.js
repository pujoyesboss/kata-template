const path = require('path');
const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser');

const port = 8080;

let app = express();
app.use(compression());
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), 'build')));

app.all('*', function(req, res) {
  res.send('build/index.html');
});

app.listen(port, () => {
  console.log(`Kata Platform is running at http://localhost:${port}/`);
  console.log('Open your browser and enter that url');
});